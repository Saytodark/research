# TCP/IP

### **WHAT IS TCP/IP?**

The Transmission Control Protocol (TCP) refers to a major Internet protocol that is responsible for the transmission or transfer of data packets over networks and the Internet. It can be classified as a connection and streaming oriented protocol.

The TCP/IP model allows reliable data exchange within a network, defining the steps to be followed from the time data is sent (in packets) to the time it is received. To achieve this, it uses a system of layers with hierarchies (a layer is built after the previous one) that communicate only with its upper layer (to which it sends results) and its lower layer (to which it requests services).

### **LAYERS TCP/IP**
![](https://www.guru99.com/images/1/093019_0615_TCPIPModelW1.png)

* #### ***NETWORK INTERFACE***

is the first layer of the model and offers the possibility of physical access to the network, specifying how the data should be routed regardless of the type of network used.

* #### ***INTERNET***

provides the data packet or datagram and manages the IP addresses. (Datagrams are data packets that constitute the minimum information in a network). ) This layer is considered the most important and includes protocols such as IP, ARP, ICMP, IGMP and RARP.

* #### ***TRANSPORT***

They allow to know the transmission status as well as the routing data and use the ports to associate an application type with a data type.

* #### ***APPLICATION***

This layer provides a set of interfaces for applications that acquire access to network services that support applications directly. Each application entity in TCP is composed of a set of functions that it needs to support the distributed communications service.

### **ADVANTAGES**

* It is capable of working on a wide range of hardware and supports many operating systems (it is multi-platform). The Internet is full of small networks with their own protocols so the use of TCP/IP has been standardized and it is possible to use it as a communication protocol between private intranet and extranet networks, facilitating a more homogeneous network.
* TCP/IP is suitable for both large and medium-sized networks as well as for business or home networks.
* TCP/IP is designed for routing and has great compatibility with standard tools for analyzing and monitoring the operation of a network.
* It is the standard protocol used worldwide to connect to the Internet and web servers.

### **DISADVANTAGES**

* Does not distinguish well between interfaces, protocols and services which affects the development of new technologies based on TCP/IP.
* On low traffic volume networks it can be slower (on higher traffic volume networks, which need a lot of routing, it can be much faster).
* When used on file servers or print servers they do not offer great performance.

# OSI

### **WHAT IS OSI?**

It is the descriptive network model proposed by the International Organization for Standardization (ISO).
It therefore constitutes a reference framework for the definition of communication system interconnection architectures.
Its function was to standardize or serialize communications on the Internet

### **LAYERS OSI**
![](https://images.idgesg.net/images/article/2017/12/osimodel-100743439-large.jpg)

* #### ***PHYSICAL***
The lowest layer of the model, deals with the network topology and the global connections between the computer and the network, referring both to the physical environment and to the way information is transmitted. It fulfils the functions of specifying the information on the physical environment (types of cable, microwave, etc.), defining the information on the electrical voltage of the transmission, the functional characteristics of the network interface and ensuring the existence of a connection (but not the reliability of the connection).

* #### ***DATA LINK***

It deals with physical redirection, error detection, media access and flow control during communication, being part of the creation of basic protocols to regulate the connection between computer systems.

* #### ***NETWORK***

This is the layer that identifies the routing between the networks involved. Thus, the data units are called "packets" and can be classified according to the routing protocol or routing protocol they use. The former select the routes (RIP, IGRP, EIGRP, among others) and the latter travel with the packets (IP, IPX, APPLETALK, etc.). The objective of this layer is to ensure that the data reaches its destination, even if this means using intermediate devices, such as routers.

* #### ***TRANSPORT***

This is where the data inside each packet is transported from the source computer to the destination computer, regardless of the physical medium used. It works through logical ports and shapes the so-called IP Sockets: Port.

* #### ***SESSION***

It is responsible for controlling and maintaining the link between the computers that exchange data, ensuring that, once communication between the two systems is established, the data transmission channel can be resumed in the event of an interruption. These services may be partially or totally dispensable, depending on the case.

* #### ***PRESENTATION***

This layer deals with the representation of the information, that is, its translation, ensuring that the data received at any end of the network is fully recognizable, regardless of the type of system used. It is the first layer that deals with the content of the transmission, rather than how it is established and sustained. In addition, it enables data to be encrypted and encoded, as well as compressed and matched to the machine receiving it (a computer, tablet, cell phone, etc.).

* #### ***APPLICATION***

Since new communication protocols are continually being developed, as new applications emerge, this last layer defines the protocols used by the applications for data exchange and allows them to access the services of any of the other layers. Generally, this whole process is invisible to the user, who rarely interacts with the application level, but with programs that interact with the application level, making it less complex than it really is.

### **ADVANTAGES**
* Helps you standardize router, switch, motherboard and other hardware
* It reduces complexity and standardizes interfaces.
* Facilitates modular engineering.
* Helps you ensure interoperable technology.
* Helps you accelerate evolution.
* Protocols can be replaced by new protocols when technology changes.
* Provide support for connection-oriented services as well as offline service.
* It is a standard model in computer networks.
* Supports offline and connection-oriented services.
* Provides flexibility to accommodate various types of protocols.

### **DISADVANTAGES**

* Adapting protocols is a tedious task.
* You can only use it as a reference model.
* It does not define any specific protocol.
* In the OSI network layer model, some services are duplicated at many layers, such as the transport and data link layers.
* The layers cannot work in parallel, as each layer must wait to get data from the previous layer.

# DIFFERENCES BETWEEN OSI AND TCP/IP 

![](https://www.guru99.com/images/1/102219_1135_TCPIPvsOSIM1.png)

* OSI has 7 layers while TCP/IP has 4 layers.
* The OSI model is a logical and conceptual model that defines the network communication used by open systems to interconnect and communicate with other systems. On the other hand, TCP/IP helps you determine how a specific computer should be connected to the Internet and how it can transmit between them.
* The OSI header is 5 bytes, while the size of the TCP/IP header is 20 bytes.
* OSI refers to the Open Systems Interconnection, while TCP/IP refers to the Transmission Control Protocol.
* OSI follows a vertical approach while TCP/IP follows a horizontal approach.
* The OSI model, the transport layer, is only connection-oriented, while the TCP/IP model is connection-oriented and connectionless.
* The OSI model is developed by ISO (International Organization for Standardization), while the TCP model is developed by ARPANET (Advanced Research Projects Agency Network).
* The OSI model helps you to standardize router, switch, motherboard and other hardware, while TCP/IP helps you to establish a connection between different types of computers.